var studentsAndPoints = [
  'Алексей Петров',0,
  'Ирина Овчинникова',60,
  'Глеб Стукалов',30,
  'Антон Павлович',30,
  'Виктория Заровская',30,
  'Алексей Левенец',70,
  'Тимур Вамуш',30,
  'Евгений Прочан',60,
  'Александр Малов',0];

function printIndent() // чтобы отделить задания друг от друга
{
  console.log('\n\n');
}

// 1. Создание массивов students и points

var students = studentsAndPoints.filter(function(item,index)
{
  return index%2==0;
}),
points = studentsAndPoints.filter(function(item,index)
{
  return index%2!=0;
});

// 2. Вывод списка студентов

console.log('Список студентов:');
students.forEach(function(item,index)
{
  console.log('Студент %s набрал %d баллов',item,points[index]);
});
printIndent();

// 3. Студент, набравший наибольшее количество баллов

var maxIndex, maxPoints;

points.forEach(function(item,index)
{
  if (!maxPoints || item>maxPoints)
  {
    maxPoints = item;
    maxIndex = index;
  }
});
console.log('Студент набравший максимальный балл: %s (%d баллов)',students[maxIndex],maxPoints);
printIndent();

// 4. Увеличение баллов двум студентам

points = points.map(function(item,index)
{
  var student = students[index];
  return student=='Ирина Овчинникова' || student=='Александр Малов' ? item + 30 : item;
});

// 5. Дополнительное задание: функция getTop

function getTop(count)
{
  var result = [];
  students.map(function(item,index)
  {
    return {student: item, point: points[index]};
  }).sort(function(a,b)
  {
    return a.point>b.point ? -1 : 1;
  }).forEach(function(item)
  {
    result.push(item.student,item.point);
  });
  return result.filter(function(item,index)
  {
    return index<count*2;
  });
}

var top3 = getTop(3), top5 = getTop(5);

function printTopItem(item,index,array)
{
  if (index%2==0)
  {
    console.log('%s – %d баллов',item,array[index+1]);
  }
}

console.log('Топ 3:');
top3.forEach(printTopItem);
console.log('Топ 5:');
top5.forEach(printTopItem);